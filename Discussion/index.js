// JavaScript Statements:
	/*
	>> Set of instructions that we tell the computer / machine to perform
	>> JS Statements usually ends with semicolon (;) to locate where the statements end
	*/


console.log("Hello once more");

//Comments

// - one-line comment (ctrl + /)
/* - multi-line comment (ctrl + shift + /) */

/*
	>> Comments are parts of the code that gets ignored by the language
	>> Comments are meant to describe the written code
*/

// Variables
	// Use for containing data
/*
	Syntax:
		let / const variableName;

*/


// Initialize Variables
let hello;
console.log(hello); // result: undefined

/*console.log(x); // result: err
let x;*/
	
	// Some best practices:
// Declaring Variables
let firstName = "Jin"; //good variable name

let pokemon = 25000; //bad variable name

let FirstName = "Izuku"; //bad variable name
let firstNameAgain = "All Might"; //good variable name

/*let first name = "Midoriya"; //bad variable name*/

//camelcase
	//lastName, emailAddress, mobileNumber
//underscores

let product_description = "lorem ipsum";
let product_id = "250000ea1000";

/*
	Syntax:
		let / const variableName = value;

*/

let productName = "desktop computer";
console.log(productName); //result: desktop computer

let productPrice = 18999;
console.log(productPrice); //result: 18999

const pi = 3.14;
console.log(pi); //result: 3.14

// Reassigning variable

/*
	Syntax:
		variableName = value
*/
productName = "Laptop";
console.log(productName); // result: Laptop

let friend = "Kate";
friend = "Chance";
console.log(friend); // result: Chance

/*let friend = "Jane";
console.log(friend); // result: err "identifier friend has already been declared"*/

/*pi = 3.1;
console.log(pi); // result: err "assignment to constant variable"*/

// Reassigning versus Initializing variable

// initialize variable
let supplier;
supplier = "Jane Smith Tradings";
console.log(supplier); // result: Jane Smith Tradings


// reassignment
supplier = "Zuitt Store";
console.log(supplier); // result: Zuitt Store


// var versus let / const

a = 5;
console.log(a); // result: 5
var a; // var has hoisting behavior

/*b = 6;
console.log(b); // result: err
let b;*/


// let / const local or global scoping

let outerVariable = "hello";

{
	let innerVariable = "hello again";
}

console.log(outerVariable); // result: hello
/*console.log(innerVariable); // result: innerVariable is not defined*/

// Multiple Variable Declarations
let	productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand); // result: DC017, Dell >> not a good practice

// let is a reserved keyword

/*const let = 'hello';
console.log(let); //result: err*/


// Data Types
// String

let country = 'Philippines';
let city = "Manila City";

console.log(city, country); // result: Manila City Philippines

// Concatenating Strings

let fullAddress = city + ',' + ' ' + country;
console.log(fullAddress); // result: Manila City, Philippines
console.log(city + ',' + ' ' + country); // result: Manila City, Philippines

let myName = "Dennis Nuval";
let greeting = "Hi I am " + myName;
console.log(greeting); // result: Hi I am Dennis Nuval

// Escape Charcter (\)
	// "\n" - create new line in between text

let mailAddress = "Metro Manila \n\nPhilippines";
console.log(mailAddress);

let message = 'John\'s employees went home early';
console.log(message); // prints the message

// Numbers
	// Integers or Whole Numbers

let	headcount = 26;
console.log(headcount);

// Decimal Numbers

let grade = 74.9;
console.log(grade); // result: 74.9


// Exponential Notation

let planetDistance = 2e10;
console.log(planetDistance); // result: 20000000000

//Boolean

let isMarried = false;
let isSingle = true;
console.log("isMarried " + isMarried);
console.log("isSingle " + isSingle);

//Arrays

/*
	Syntax:
		let / const arrayName = [elementA, elementB, ...];

*/

//similar data types
let grades = [98.7, 77.3, 90.8, 88.4];
console.log(grades);

const anime = ["BHNA", "AOT", "SxF", "KNY"];
console.log(anime);

//different data types
let random = ["JK", 24, true]; //not a good practice
console.log(random);

//Objects Data Types
/*
	Syntax:
		let / const objectName = {
	propertyA : valueA,
	propertyB : valueB
		}

*/


let person = {
	fullName: "Edward Scissorhands",
	age: 35,
	isMarried: false,
	contact: ["09123456789", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
};
console.log(person);

const myGrades = {
	firstGrading: 98.7,
	secondGrading: 77.3,
	thirdGrading: 88.6,
	fourthGrading: 74.9
};
console.log(myGrades);


//typeof Operator

console.log(typeof myGrades); // result: Object
console.log(typeof grades); // result: Object (special type of object)
console.log(typeof grade); // result: Number

/*anime = ['One Punch Man'];
console.log(anime); // result: err (assignment to constant variable)*/


anime[0] = ['One Punch Man'];
console.log(anime);


//Null

let spouse = null;
console.log(spouse); // result: null

let zero = 0;
let emptyString = '';

// Undefined

let y;
console.log(y); // result: undefined
